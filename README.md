# Artificial Intelligence Nanodegree
## Introductory Project: Diagonal Sudoku Solver

# Question 1 (Naked Twins)
Q: How do we use constraint propagation to solve the naked twins problem?  
A: In terms of constraint propagation, Naked twins is similar to Elimination.
 Elimination removes a candidate digit which is determined as a unique digit in another box in a same unit.
 Similarly, Naked twins removes candidate digits which are a pair of candidates in other two box in a same unit.
 These two methods (Elimination and Naked twins) constraint candidates of other place boxes by determining one or more boxes' candidates in a same unit.
  And then, we can propagate the constraint to other units repeatedly.


# Question 2 (Diagonal Sudoku)
Q: How do we use constraint propagation to solve the diagonal sudoku problem?  
A: Compared to original sudoku problem, the diagonal sudoku problem has two more constraints,
  which the numbers 1 to 9 should all appear exactly once among the two main diagonals. 
  We can use the new two constraints to narrow candidates of box values, and propagate the constraints to other units.  
 The more constraints are propagated, the search space is shrinked. 
 We can solve sudoku problem more efficiently by shkinking search space.
 So, adding new constraints(adding diagonal units to unitlist) is preferred.


### Install

This project requires **Python 3**.

We recommend students install [Anaconda](https://www.continuum.io/downloads), a pre-packaged Python distribution that contains all of the necessary libraries and software for this project. 
Please try using the environment we provided in the Anaconda lesson of the Nanodegree.

##### Optional: Pygame

Optionally, you can also install pygame if you want to see your visualization. If you've followed our instructions for setting up our conda environment, you should be all set.

If not, please see how to download pygame [here](http://www.pygame.org/download.shtml).

### Code

* `solutions.py` - You'll fill this in as part of your solution.
* `solution_test.py` - Do not modify this. You can test your solution by running `python solution_test.py`.
* `PySudoku.py` - Do not modify this. This is code for visualizing your solution.
* `visualize.py` - Do not modify this. This is code for visualizing your solution.

### Visualizing

To visualize your solution, please only assign values to the values_dict using the ```assign_values``` function provided in solution.py

### Data

The data consists of a text file of diagonal sudokus for you to solve.